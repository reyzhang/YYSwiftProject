# YYSwiftProject

#### 项目介绍
swift项目，仿写有妖气漫画，旨在提高swift代码能力，熟悉使用知名三方库并自定义封装一些控件

github地址：https://github.com/daomoer/YYSwiftProject

项目使用了Alamofire网络请求库，并使用Moya对请求库进行了二次封装。 HandyJson库对请求结果进行数据转模型的切换

 